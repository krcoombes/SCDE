---
title: "Protein Expression, Ley CAVA Data"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: readable
    highlight: kate
    toc: yes
  pdf_document:
    highlight: kate
    toc: yes
always_allow_html: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE, fig.width=6, fig.height=6, dev = "png")
options(width = 92)

.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Background
We have an idea that we think may lead to better quantifications of mRNA-Seq
data. We want to test it on a real-world data set. Here we use a hierarchical
Bayesian beta-binomial model to estimate the probability that an mRNA read
from a patient sample comes from a particular gene, for all 487 genes in the
data set from Klaus Ley.

```{r setPaths}
source("00-paths.R")
paths
```

# Data
We first load the data set obtained from the laboratory of Dr. Klaus Ley. 
```{r fetchData}
library(Seurat)
load(file.path(paths$scratch, "LeyData.Rda"))
load(file.path(paths$scratch, "pseudoBulk.Rda"))
load(file.path(paths$scratch, "dimred.Rda"))
load(file.path(paths$data, "seurat_classify.rda"))
load(file.path(paths$scratch, "BI.Rda"))
ls()
```

# Setup
```{r bingo}
LP <- log2(1 + protein)
temp <- sapply(strsplit(colnames(LP), "\\."),
               function(X) X[1])
colnames(LP) <- temp
rm(temp)
bingo <- 1*(sweep(LP, 2, cutpoint, "-") > 0)
ImportantMarkers <- c("CD4", "CD8", "CD19", "CD20", "CD56", "CD11c", "CD14", "CD16")
bongo <- apply(bingo[, ImportantMarkers], 1, paste, collapse = "")
length(bongo)
length(unique(bongo))
```

```{r commoners}
tab <- rev(sort(table(bongo)))
sum(tab > 0.01*nrow(bingo))
sum(tab > 0.005*nrow(bingo))
sum(tab > 0.001*nrow(bingo))
sum(tab > 0.0005*nrow(bingo))
head(tab, 18)
```

```{r}
Characterize <- function(pick) {
  punk <- sapply(1:ncol(LP), function(J) {
    mean(LP[pick,J] > cutpoint[J])
  })
  names(punk) <- colnames(LP)
  sort(round(100*punk, 1))
}

cize <- function(pick, K = 6) {
  rev(tail(Characterize(pick), K))
}

absinthe <- function(pick, K = 6) {
  head(Characterize(pick), K)
}
```

```{r}
knack <- names(tab)
xlate <- sapply(knack, function(D) {
  dex <- as.logical(as.numeric(strsplit(as.character(D), "")[[1]]))
  retval <- gsub("CD", "", paste(ImportantMarkers[dex], collapse = ","))
  if (retval == "") retval <- "unknown"
  retval
})
```

# Characterized Cell Types
```{r}
for (J in 1:100) {
  lbl <- xlate[J]
  cnt <- tab[J]
  perc <- paste(round(cnt/nrow(LP)*100, 2), "%", sep = "")
  names(lbl) <- names(cnt) <- names(perc) <- NULL
  spress <- cize(bongo == names(tab)[J], 10)
  nil <- absinthe(bongo == names(tab)[J], 10)
  out <- list(Signature = lbl, 
              Binary = names(tab)[J],
              NCells = cnt, 
              PCells = perc, 
              Expresses = spress,
              Lacking = nil)
  cat("\n\n-------------------\nType", J, "\n", file = stdout())
  print(out)
}
```

```{r saveDF}
cavaMarks <- data.frame(Signature = NA, Binary = NA, NCells = NA, PCells = NA,
                        Action = NA, Marker = NA, Percent = NA)[-1,]
for (J in 1:167) {
  lbl <- xlate[J]
  cnt <- tab[J]
  if (cnt < 5) break
  perc <- paste(round(cnt/nrow(LP)*100, 2), "%", sep = "")
  names(lbl) <- names(cnt) <- names(perc) <- NULL
  out <- list(Signature = lbl, 
              Binary = names(tab)[J],
              NCells = cnt, 
              PCells = perc)
  espresso <- cize(bongo == names(tab)[J], 20)
  espresso <- espresso[espresso > 95]
  decaf <- absinthe(bongo == names(tab)[J], 10)
  decaf <- decaf[decaf < 1]
  if (length(espresso) > 0 & length(decaf) > 0) {
    RB <- rbind(data.frame(Action = "expresses", Marker = names(espresso), Percent = espresso),
                data.frame(Action = "lacks", Marker = names(decaf), Percent = decaf))
  } else if (length(decaf) > 0) {
    RB <- data.frame(Action = "lacks", Marker = names(decaf), Percent = decaf)
  } else {
    RB <- data.frame(Action = "expresses", Marker = names(espresso), Percent = espresso)
  }
  DF <- data.frame(out, RB)
  rownames(DF) <- NULL
  cavaMarks <- rbind(cavaMarks, DF)
}
for (I in c(1,2,5,6)) cavaMarks[,I] <- factor(cavaMarks[, I])
cavaMarks$PCells <- as.numeric(sub("%", "", cavaMarks$PCells))
summary(cavaMarks)
write.csv(cavaMarks, file = file.path(paths$results, "cavaMarks.csv"))
```

```{r}
foobar <- matrix(NA, ncol = ncol(LP), nrow = length(xlate))
dimnames(foobar) <- list(xlate, colnames(LP))
for (I in 1:nrow(foobar)) {
  B <- names(xlate[I])
  bop <- Characterize(bongo == B)
  bop <- bop[colnames(foobar)]
  foobar[I,] <- bop
}
cobar <- 0*foobar
cobar[foobar > 95] <- +1
cobar[foobar < 1] <- -1
heatmap(cobar, scale="none", col = c("firebrick", "grey", "forestgreen"))
save(foobar, cobar, file = file.path(paths$scratch, "typeMats.Rda"))
write.csv(foobar, file = file.path(paths$results, "percents.csv"))
write.csv(cobar, file = file.path(paths$results, "ternary.csv"))
```

# Appendix

This analysis was performed in the following R environment:
```{r si}
sessionInfo()
```
It was also performed at the following on-disk location:
```{r cwd}
getwd()
```