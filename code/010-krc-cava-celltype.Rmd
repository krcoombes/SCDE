---
title: "Hierarchical BB models and scRNA cell types"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: readable
    highlight: kate
    toc: yes
  pdf_document:
    highlight: kate
    toc: yes
always_allow_html: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE, fig.width=6, fig.height=6, dev = "png")
options(width = 92)

.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Background
We have an idea that we think may lead to better quantifications of mRNA-Seq
data. We are test it on a real-world data set from Dr. Klaus Ley. In this
report, we want to determine the cell types present in the experiment. 

```{r setPaths}
source("00-paths.R")
paths
```

# Data
We load all the relevant data that was cached during previous analyses.
```{r fetchData}
library(Seurat)
load(file.path(paths$scratch, "LeyData.Rda"))
load(file.path(paths$scratch, "pseudoBulk.Rda"))
load(file.path(paths$scratch, "dimred.Rda"))
load(file.path(paths$data, "seurat_classify.rda"))
load(file.path(paths$scratch, "BI.Rda"))
ls()
```

# Seurat (Flawed) Cell Types
Jake Reed (when he was first learning to use Seurat) ran an analysis using
the `scAnnotatoR` package to find cell types. However, this analysis was
flawed because it mixed mRNA and protein measurements together 
indiscriminately.  So, we are going to explore this problem in more depth,
but restricting our attention to the protein surface markers used in this data
set. Reed's cell type assignments are contained in a Seurat object, which we
now start to explore.
```{r oldCellTypes}
class(seurat)
slotNames(seurat)
class(seurat@assays)
length(seurat@assays)
M <- seurat@meta.data
class(M)
dim(M)
tail(colnames(M)) # one of the last two columns should be cell types
lost <- !(rownames(M) %in% rownames(protein))
lostCells <- M[lost,]
MU <- M[rownames(protein),82:83]
typef <- MU[, 2]
sort(table(typef))
```
The one `CD16 Mono` and three `Epithelial cells` are unlkely to be useful
(and maybe not even meaningful). But we also note that there are still two
kinds of monocytes (`CD14 Mono` and `Monocytes`) and three kinds of NK
cells (`NK`, `CD16 NK`, and `CD56 NK`), where it is unclear how they really
differ.

## Charactizing Cell Subsets
We previously applied models of bimodality to define cutpoints that
distinguished clear (high) expression of a protein from dim or nonexistent
expression. We can use those cutpoints to characterize cell subsets by the
fraction of cells (highly) expressing ech protein. We also define an
intrinsic expression measure by looking at the ration of the mean to the
maximum expression of each protein. 
```{r LP-Characterize}
tmp <- sapply(strsplit(colnames(protein), "\\."), function(X) X[1])
LP <- log2(1 + as.matrix(protein))
colnames(LP) <- tmp
colnames(protein) <- tmp

Characterize <- function(pick) {
  punk <- sapply(1:ncol(LP), function(J) {
    mean(LP[pick,J] > cutpoint[J])
  })
  names(punk) <- colnames(LP)
  sort(round(100*punk, 1))
}

cize <- function(pick, K = 6) {
  tail(Characterize(pick), K)
}

peek <- function(typ, K = NULL) {
  X <- LP[typef == typ, , drop = FALSE]
  M <- apply(X, 2, mean)
  AX <- apply(X, 2, max)
  if (is.null(K)) K <- sum(M/AX > 0.5)
  rev(sort(round(M/AX, 3)))[1:K]
}
```

## NK Cells
We begin by illustrating the three kinds of NK cells that are claimed to
be identified by the previous analysis (**Figure 1**).
```{r fig01, fig.cap=.tag(1, "NK cell subtypes?"), fig.width=14, fig.height = 7}
showNK <- typef
showNK[!(showNK %in% c("NK", "CD16 NK", "CD56 NK"))] <- "other"
showNK <- factor(showNK)
rgbg <- c("red", "green", "blue", "grey")
tempcols <- rgbg[as.numeric(showNK)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = tempcols, main = "UMAP")
plot(TT$Y, pch = '.', col = tempcols, main = "t-SNE")
foo <- par(mai = c(0.8, 0, 1.0, 0))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(showNK), pch = 15, col = rgbg, cex = 1.5)
par(foo)
par(opar)
par(mfrow = c(1, 1))
```
If we examine these plots closely, the green `CD56 NK` and the red 
`CD16 NK` cells are not really separable. However, some of the blue
cells simply labeled as `NK` are on the borders of other larger natural
cluster of cells.

```{r NK}
cize(typef == "NK")
cize(typef == "CD16 NK")
cize(typef == "CD56 NK")

peek("NK")
peek("CD56 NK")
peek("CD56 NK")
```
I am not at all convinced that there are NK cells that don't express CD56,
which is what a lot of the cells just labeled "`NK`" represent. My plan is
to combine the CD16 and CD56 subsets, and rename the other group for now
as  "NK-like", with some hesitation about that call.

## Monocytes
Next, we look at the two monocyte cell types that scAnnotatoR reported
(**Figure 2**).
```{r fig02, fig.cap=.tag(2, "Monocyte subtypes?"), fig.width=14, fig.height = 7}
showMO <- typef
showMO[!(showMO %in% c("Monocytes", "CD14 Mono"))] <- "other"
showMO <- factor(showMO)
rgbg <- c("red", "blue", "grey")
tempcols <- rgbg[as.numeric(showMO)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = tempcols, main = "UMAP")
plot(TT$Y, pch = '.', col = tempcols, main = "t-SNE")
foo <- par(mai = c(0.8, 0, 1.0, 0))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(showMO), pch = 15, col = rgbg, cex = 1.5)
par(foo)
par(opar)
par(mfrow = c(1, 1))
```
Ths appears to be another "distinction without a difference". We should just
combine these two groups into one.

```{r Mono}
cize(typef == "CD14 Mono")
cize(typef == "Monocytes")
peek("CD14 Mono")
peek("Monocytes")
```

## T Cells
Now we want to explore the vraios T cell subsets (**Figure 3**).
```{r fig03, fig.cap=.tag(3, "T cell subtypes."), fig.width=14, fig.height = 7}
showT <- typef
showT[!(showT %in% c("T cells", "CD4 T cells", "CD8 T cells", "Treg", "NKT"))] <- "unassigned"
showT <- factor(showT)
rgbg <- c("red", "green", "blue", "orange", "purple", "grey")
tempcols <- rgbg[as.numeric(showT)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = tempcols, main = "UMAP")
plot(TT$Y, pch = '.', col = tempcols, main = "t-SNE")
foo <- par(mai = c(0.8, 0, 1.0, 0))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(showT), pch = 15, col = rgbg, cex = 1.5)
par(foo)
par(opar)
par(mfrow = c(1, 1))
```

We can easily see the large CD4 and CD8 T cell subsets, but the smaller groups
are hidden in this plot. So, we are gonig to just look at those smaller
subsets (**Figure 4**).

```{r fig04, fig.cap=.tag(4, "T cell subtypes."), fig.width=14, fig.height = 7}
showT[!(showT %in% c("T cells", "Treg", "NKT"))] <- "unassigned"
showT <- factor(showT)
rgbg <- c("blue", "orange", "magenta", "grey")
tempcols <- rgbg[as.numeric(showT)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = tempcols, main = "UMAP")
plot(TT$Y, pch = '.', col = tempcols, main = "t-SNE")
foo <- par(mai = c(0.8, 0, 1.0, 0))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(showT), pch = 15, col = rgbg, cex = 1.5)
par(foo)
par(opar)
par(mfrow = c(1, 1))
```

```{r Tcells}
cize(typef == "CD4 T cells")
cize(typef == "CD8 T cells")
cize(typef == "T cells")
cize(typef == "Treg")
cize(typef == "NKT")

peek("CD4 T cells")
peek("CD8 T cells")
peek("T cells")
peek("Treg")
peek("NKT")
```

One of the differences between these groups may be explained by the CD45
isoform, with the two big groups having CD45RA and the three smaller groups
mostly having CD45RO. That typically indicates a difference in maturity, but
it may not be a strong enough signal to drive the UMAP or t-SNE plots.

# B Cells
After B cells get done killing off an invader, they tend to differentiate 
either into plasma cells or memoyt B cells. The `scAnnotatoR` clusters clai
to identify both B cells and plasma ells, but do not distinguish memory b cels.
Here we take a look at the two classes theyt predict (**Figure 5**).
```{r fig05, fig.cap=.tag(5, "B cell subtypes."), fig.width=14, fig.height = 7}
showB <- as.character(typef)
showB[!(showB %in% c("B cells", "Plasma cells"))] <- "unassigned"
showB <- factor(showB)
rgbg <- c("red", "blue", "grey")
tempcols <- rgbg[as.numeric(showB)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = tempcols, main = "UMAP")
plot(TT$Y, pch = '.', col = tempcols, main = "t-SNE")
foo <- par(mai = c(0.8, 0, 1.0, 0))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(showB), pch = 15, col = rgbg, cex = 1.5)
par(foo)
par(opar)
par(mfrow = c(1, 1))
```
We find that the regions of plasma cells and B cells overlap; so we
suspect that their classification is wrong. We especiall think that
because both the UMAP and t-SNE plots have more than one region that
represent B cells.


```{r Bcells}
cize(typef == "Plasma cells")
cize(typef == "B cells")

peek("Plasma cells")
peek("B cells")
```

## Modified Seurat Cell Types
We now implement the modifications that we decided on in the previous sections.
```{r modify}
typef[typef %in% c("CD16 Mono", "CD14 Mono")] <- "Monocytes"
typef[typef == "Endothelial cells"] <- "unknown"
typef[typef %in% c("CD16 NK", "CD56 NK")] <- "NK cells"
typef[typef == "NK"] <- "NK-like"
typef[typef == "Plasma cells"] <- "B cells"
typef <- as.factor(typef)
```

We also (manually) define colors for each of the current cell type.
```{r colorScheme}
library(Polychrome)
myp <- rep(NA, 16)
names(myp) <- levels(typef)
myp["unknown"] <- "gray50"
myp["B cells"] <- "cornflowerblue"
myp["T cells"] <- "orangered2"
myp["Treg"] <- "darkorange2"
myp["CD4 T cells"] <- "lightsalmon1"
myp["CD8 T cells"] <- "goldenrod3"
myp["NKT"] <- "limegreen"
myp["NK cells"] <- "cyan4"
myp["NK-like"] <- "brown4"
myp["Monocytes"] <- "purple"
myp["Mast cells"] <- "forestgreen"
myp["VEC"] <- "darkolivegreen3"
myp["DC"] <- "deeppink"
myp["ILC"] <- "red2"
myp["Platelets"] <- "magenta2"
myp["LEC"] <- "royalblue1"


```

In **Figure 6**, we try to show all 17 of the remaining (somewhat consolidated)
cell types. We can easily see, B cells, CD4 and CD8 T cells, NK cells, monocytes,
and the cells we are currently calling "NK-like". But most of the rarer subtypes
seem to be hidden.
```{r fig06, fig.cap=.tag(6, "Modified scAnnotatoR cell types."), fig.width = 14, fig.height=7}
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = '.', col = myp[as.numeric(typef)], main = "UMAP")
plot(TT$Y, pch = '.', col = myp[as.numeric(typef)], main = "t-SNE")
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(typef), pch = 15, col = myp, cex = 1.5)
par(opar)
par(mfrow = c(1, 1))
ignore = dev.copy(png, file = file.path(paths$figures, "seuratTypes.png"),
                  width=14*300, height = 6*300, res = 300, bg = "white")
ignore = dev.off()
rm(ignore)
```

We now compute the mean expression of each protein for each cell type, and use
those values to create a heatmap (**Figure 7**). Some of the properties we expect
to see are present here:

* CD T cells are characterized by high expression of CD8.
* NK cells are characterized by high expression of CD16 and CD56.
* Monocytes have high expressio of CD11b and C14.
* Both B cells and plasma cells have high expression of CD19, CD20, IgM,
  IgD, and, less obviously, CD24, CD185, and CD196,
* Platelets have high expression of CD154, CD9, CD69, CD6, CD194, and CD24.

Surprisingly, CD4 T cells have high expression of CD197 and only moderate levels
of CD4. Many other cell typoes don't have clearly dominant markers of any sort.

```{r fig07, fig.width=10, fig.height=10, fig.cap=.tag(7, "Heatmap of mean expression of proteins by cell type.")}
aggie <- aggregate(LP, list(typef), mean)
rownames(aggie) <- aggie[,1]
aggie <- aggie[, -1]
heatmap(t(aggie), scale = "row", cexRow = 0.6, col = viridisLite::viridis(64))
```

# Clustering Of (Not Using) Proteins
## By Correlation
We want to compute the correlation between protein expression, and use it as a
similarity measure to cluster the proteins (**Figure 8**).
```{r fig08, fig.width=12, fig.cap=.tag(8, "Dendrogram clustering proteins by correlation.")}
library(Mercator)
coy <- distanceMatrix(LP, "pearson")
M <- Mercator(coy, "correlation", "hclust", K = 9)
opar <- par(mai = c(1.3, 0.3, 0.8 ,0.3))
plot(M, view = "hclust", cex = 1.3, main = "Correlation Distanmce")
par(opar)
```

## By Sokal-Michener Binary Distance
We can convert the protein data to binary (where 1 = expressed and 0 = unexpressed based on the cutpoints
defined earlier). Then we can use one of the many standard methods to measure the difference between binary
vectors. Since expression is not particularly rare in this context, we probabyl want to use a measure that 
treats "off-matches" as though they are just as important as "on-matches. The simplest way to do that is
to use the "Sokal-Michener" distance, which is defined as
$$1 - \frac{N_{00} + N){11}}{N_{00} + N_{01} + N_{10} + N_{11}}.$$ 
The clustering results are shown in **Figure 9**.
```{r fig09, fig.width=12, fig.cap=.tag(9, "Dendrogram clustering proteins by the Sokal-Michener binary distance.")}
bingo <- 1*(sweep(LP, 2, cutpoint, "-") > 0)
table(apply(bingo, 1, sum))
jack <- binaryDistance(bingo, "sokal")
M <- Mercator(jack, "sokal", "hclust", 10)
opar <- par(mai = c(1.3, 0.3, 0.8 ,0.3))
plot(M, view = "hclust", cex=1.3, main = "Sokal-Michener binary distance.")
par(opar)
```


# Defining Our Own Cell Types
Working in part from the protein expression plots in Report `krc04`, along
with information gleaned from Internet searches, especially those coming from
a BioRad pamphlet 
(https://www.bio-rad-antibodies.com/human-immune-cell-marker-expression-antibodies-poster-guide.html) 
describing their antibody panels for flow cytometry and a series of HTML
guides from BioCompare (https://www.biocompare.com/), we want to define our own cell types.

We define some functions that make it easier to define cell types based on the
presence or absence of specific protein markers

```{r typetools}
With <- function(...) {1
  S <- sapply(list(...), function(mark) {
    J <- which(colnames(LP) == mark)
    if (length(J) != 1) {
      stop("Did not define aunique column!")
    }
    LP[, J] > cutpoint[J]
  })
  apply(S, 1, all)
}
ButNot <- function(...) {
  S <- sapply(list(...), function(mark) {
    J <- which(colnames(LP) == mark)
    if (length(J) != 1) {
      stop("Did not definea unique column!")
    }
    LP[, J] < cutpoint[J]
  })
  apply(S, 1, all)
}
```


We are going to determine the available combinations of expression of what we believe
to be the most important markers:
```{r mymarkers}
ImportantMarkers <- c("CD4", "CD8", "CD19", "CD20", "CD56", "CD11c", "CD14", "CD16")
```

These markers should allow us to identify (at least) B cells, CD4 and CD8 T cells,
NK cells, and monocytes. We use them to convert the binary expression matrix for all markers
```{r variedMarkers}
bongo <- apply(bingo[, ImportantMarkers], 1, paste, collapse = "")
length(bongo)
length(unique(bongo))
```
There are 167 distinct patters of expression, but we want to restrict our attention to
the most common ones. Here we defined "most common" to mean accounting for at least 1% of
the cells in the data set
```{r commoners}
tab <- rev(sort(table(bongo)))
sum(tab > 0.01*nrow(bingo))
head(tab, 18)
```
Interestingly, one of the most common patterns (`00000000`) consists of the 4,968 cells that
do not express any of our chosen markers. Here are the markers that these cells may express
(where the numbers are the percentages of those cells with that marker):
```{r nulls}
cize(bongo == "00000000", 10)
```

We now want to convert the binary strings back to the names of CD markers.
```{r backtrack}
wrongo <- bongo
wrongo[!(wrongo %in% names(tab)[1:14])] <- "rare" # Things that account for less than 1% of cells
wrongo[wrongo == "00000000"] <- "unknown" # Things that express none of our favorite markers.
xlate <- sapply(wrongo, function(D) {
  if (D %in% c("rare", "unknown")) {
    retval <- D
  } else {
    dex <- as.logical(as.numeric(strsplit(as.character(D), "")[[1]]))
    retval <- gsub("CD", "", paste(ImportantMarkers[dex], collapse = ","))
  }
  retval
})
wrongo <- factor(xlate)
levels(wrongo)
```

```{r wrcd}
cize(wrongo == "rare", 10)
```

We can immediately interpret some of these kinds of cells.

* `19,20` = "B cells".
* `4` and `8` = two most common classes of T cells.
* `16,56` = NK cells.

We can also see which other markers commonly go along with the defining patterns.
```{r argybargy}
for (L in levels(wrongo)) {
  if (L == "rare") next
  if (L == "unknown") {
    cize(do.call(ButNot, as.list(ImportantMarkers)))
    next
  }
  wit <- sapply(strsplit(L, ",")[[1]], function(W) paste("CD", W, sep = ""))
  bun <- ImportantMarkers[!(ImportantMarkers %in% wit)]
  cat("\n", L, "\n", file = stdout())
  print(cize(do.call(With, as.list(wit)) & do.call(ButNot, as.list(bun)), 10))
}
```

* `11c` = dendritic cells?
* `11c,14` = classical monocytes (since they are negative for CD14)
* `11c,16` = intermediate monocytes or macrophages?
* `11c,14,16` = Nonclassical monocyte?
* `20,11c,14` = Memory B cells?
* `56`
* `56,11c,14`
* `56,11c,16`
* `8,56` = NKT cells?


Now we create UMAP and t-SNE plots for the clusters defined this way (**Figure 10**).
```{r recolor}
set.seed(82460)
mop <- gplots::col2hex(myp)
fudge <- createPalette(22, mop, range = c(30, 65))
wcol <- fudge[c(1, 8, 11, 13, 6, 
                7, 14, 16, 18, 21, 
                10, 3, 9, 5, 15)]
names(wcol) <- levels(wrongo)
swatch(wcol)
colvec <- wcol[as.numeric(wrongo)]
```

```{r fig10, fig.cap=.tag(10, "Our cell types, by limited surface markers."), fig.width=14, fig.height = 7}
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = ".", col = colvec, main = "UMAP", cex=2)
plot(TT$Y, pch = ".", col = colvec, main = "t-SNE", cex=2)
op <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(wrongo), pch = 15, col = wcol, cex = 2)
par(op)
par(opar)
par(mfrow = c(1, 1))
ignore = dev.copy(png, file = file.path(paths$figures, "myTypes.png"),
                  width=14*300, height = 6*300, res = 300, bg = "white")
ignore = dev.off()
rm(ignore)
```

Not surprisingly, the moderately rare subtypes are hard to see in this plot. So, we are going to gray
out the common subtypes that we fully undertand, and just color the rest of the cell types..
```{r fig11, fig.cap=.tag(11, "Highlighting less common cell types."), fig.width=14, fig.height = 7}
ucol <- wcol
ucol[c(2, 5, 7, 11, 12)] <- "grey85"
colvec <- ucol[as.numeric(wrongo)]
layout(matrix(c(1, 3, 2), nrow = 1), width = c(5,2,5))
opar <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(UU$layout, pch = ".", col = colvec, main = "UMAP", cex=2)
plot(TT$Y, pch = ".", col = colvec, main = "t-SNE", cex=2)
op <- par(mai = c(0.8, 0.3, 1.0, 0.3))
plot(0, 0, type = "n", axes = FALSE)
legend("topleft", levels(wrongo), pch = 15, col = ucol, cex = 2)
par(op)
par(opar)
par(mfrow = c(1, 1))
ignore = dev.copy(png, file = file.path(paths$figures, "moderate.png"),
                  width=14*300, height = 6*300, res = 300, bg = "white")
ignore = dev.off()
rm(ignore)
```

```{r}
table(typef, wrongo)
```

```{r ourCT}
ourCellTypes <- wrongo
cellTypeColors <- wcol
ctVector <- cellTypeColors[as.numeric(ourCellTypes)]
save(ourCellTypes,cellTypeColors, ctVector, 
     file = file.path(paths$scratch, "ourCellTypes.Rda"))
```

# Appendix

This analysis was performed in the following R environment:
```{r si}
sessionInfo()
```
It was also performed at the following on-disk location:
```{r cwd}
getwd()
```
