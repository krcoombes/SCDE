---
title: "CD4T Data Sources"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    highlight: kate
    theme: readable
    toc: yes
  pdf_document:
    highlight: kate
    toc: yes
---

```{r opts, echo=FALSE}
knitr::opts_chunk$set(fig.width=6, fig.height=5)
options(width=96)
.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
caption { font-weight: bold; color: black; }
</style>
')
```

# Background
We recently received single-cell data for CD4 T cells from members of the Ley lab.
In this report, we would like to try to reproduce at least some aspects of UMAP
analyses that they performed previously.

# Methods and Results
We start by loading the Seurat library package so we can make sense of the saved
data objects.
```{r libpack}
suppressWarnings( library(Seurat) )
```

In a previous report, we studied how data in a `seurat` object stored in a file
called "seurat_classify.rda" was related to data stored in a file called
"CAVA_CD4TCells.RDS". The `seurat` data contained merged raw count mRNA and protein 
data on a complete set of >160,000 PBMC cells. The `cava` data kept the mRNA and
protein data in separate slots, but only contained data on ~40,000 CD4-positive T Cells.
here we are going to restrict our attention to the CD4 T Cells.
```{r cava}
cava <- readRDS("CAVA_CD4TCells.RDS")
cava
```

## Metadata
The clinical and demographic data about the source of the cells is contained in the 
"meta.data" slot.
```{r md}
md <- cava@meta.data
colnames(md)
head(md[, 19:27])
```
There are a host of different "clustering" columns in this data set. 
```{r cluster}
attach(md)
table(Cluster)
table(ADT_snn_res.0.6)
table(ADT_snn_res.0.4)
table(seurat_clusters)
table(clustering_snn_res.0.25)
detach()
```

And there is a separate slot called "clustering". This slot contains data for each of the
40,816 cells for 34 rows=proteins. The difference between normalized and scaled data is a
separate liner transformation for each row, together with truncation of the top level at 10
(**Figure 1**).
```{r clustering_slot}
slotNames(X <- cava@assays$clustering)
dim(X@counts) # 0  0
dim(X@data)       # 34 40816
dim(X@scale.data) # 34 40816
```

```{r fig01, fig.cap = .tag(1, "Clustering data set."), fig.width=8, fig.height=8}
plot(X@data, X@scale.data, col = 1:34, xlab = "normalized", ylab = "scaled")
for (J in 1:34) {
  okay <- X@scale.data[J,] < 10
  cf <- coef(lm(X@scale.data[J, okay] ~ X@data[J, okay]))
  if(!any(is.na(cf))) {
    abline(cf, col = J)
  }
}
```
So, it is clear that the default truncation rule (`scale.max = 10`) is unreasonable in
this data set.

```{r}


range(as.vector(cava@assays$ADT@counts))
range(as.vector(cava@assays$ADT@data))
range(as.vector(cava@assays$ADT@scale.data))

range(as.vector(cava@assays$thAb@counts))
range(as.vector(cava@assays$thAb@data))
range(as.vector(cava@assays$thAb@scale.data))

plot(cava@assays$ADT@data, cava@assays$thAb@data)
abline(0,1, col = "red")
plot(cava@assays$ADT@data, cava@assays$thAb@counts)
plot(cava@assays$thAb@data, cava@assays$thAb@counts)

all(cava@assays$ADT@data == cava@assays$thAb@counts)
```

```{r echo=FALSE, eval = FALSE}
library(ClassDiscovery)
spca <- SamplePCA(cava@assays$ADT@data)
```

They are clearly different (**Figure 1**)
```{r fig02, fig.cap = .tag(2, "Cross-classification for two clustering columns in the CAVA meta data..")}
summary(md$clustering_snn_res.0.25)
summary(as.factor(md$Cluster))
tab <- table(md$Cluster, md$clustering_snn_res.0.25)
image(1:17, 0:19, tab, xlab="Unknown cluster algorithm", ylab = "SNN clustering")
rm(tab)
```
Since we have been working with clusters with names like "CD4T_17", in the "Cluster" column,
that will be the one we try first.

## Color Scheme
We set up a new "default" color scheme to maximize our chances of being able to see 17 
or more distinct colors.
```{r de24}
library(Polychrome)
data("Dark24")
cad <- computeDistances(Dark24)
d24 <- Dark24[names(cad)]
de24 <- c(d24[-10], d24[10])
names(de24) <- colorNames(de24)
rm(cad, d24, Dark24)
```

```{r fig03, fig.cap = .tag(3, "Color scheme."), fig.width=7, fig.height = 4}
swatch(de24)
```


```{r echo=FALSE, eval = FALSE}
dim(cava@assays$RNA@scale.data)  # 477 x 40816
dim(cava@assays$ADT@scale.data)  #  51 x 40816
dim(cava@assays$thAb@scale.data) #  51 x 40816

all.equal(cava@assays$ADT@scale.data, cava@assays$thAb@scale.data) # cross entries
plot(cava@assays$ADT@scale.data, cava@assays$thAb@scale.data, col = 1:51) # grossly different
## corresponding rows appear to be (separately) linearly related
## some rows in thAb have been set to (different) constants
## seems to be based on maximum value

## We will use the ADT data
plot(cava@assays$ADT@counts, cava@assays$ADT@data, col = 1:51)     # normalization
## Normalization clealy has a strong effect
plot(cava@assays$ADT@data, cava@assays$ADT@scale.data, col = 1:51) # scaling
## Scaling is less dramatic, but gives separate linear adjustments to each
## row, meaning each antibody.
## Should be able to reconstruct the scaling parameters, and thus the algorithm

library(ClassDiscovery)
f <- "ed2.Rda"
if (file.exists(f)) {
  load(f)
} else {
  ed.raw <- distanceMatrix(t(as.matrix(cava@assays$ADT@counts)), "pearson")
  ed.norm <- distanceMatrix(t(as.matrix(cava@assays$ADT@data)), "pearson")
  ed.scaled <- distanceMatrix(t(as.matrix(cava@assays$ADT@scale.data)), "pearson")
  save(ed.raw, ed.norm, ed.scaled, file = f)
}
rm(f)

library(Mercator)
f <- "planet2.Rda"
if (file.exists(f)) {
  load(f)
} else {
  planet <- Mercator(ed.raw, "pearson", "hclust", K = 5)
  planet <- addVisualization(planet, "mds")
  planet <- addVisualization(planet, "tsne", perplexity = 15)
  planet <- addVisualization(planet, "umap")
  planet <- addVisualization(planet, "som",
                             grid = kohonen::somgrid(6, 5, "hexagonal"))
  planet@palette <- de24
  save(planet, file = f)
}
rm(f)

opar <- par(mfcol = c(2,3))
barplot(planet)
plot(planet, view = "hclust")
plot(planet, view = "mds", main="MDS; Correlation Distance", cex = 2)
plot(planet, view = "tsne", main="t-SNE; Correlation Distance", cex=2)
plot(planet, view = "umap", main="UMAP from distance matrix", cex=2)
plot(planet, view = "som")
par(opar)

if (file.exists("ted_scaled.Rds")) {
  ted.scaled <- readRDS("ted_scaled.Rds")
} else {
  ted.scaled <- dist(t(cava@assays$ADT@scale.data)) # scaled
  saveRDS(ted.scaled, file = "ted_scaled.Rds")
}
if (file.exists("ted_norm.Rds")) {
  ted.norm <- readRDS("ted_norm.Rds")
} else {
  ted.norm <- dist(t(cava@assays$ADT@scale.data)) # normalized
  saveRDS(ted.norm, file = "ted_norm.Rds")
}
if(file.exists("ted_raw.Rda")) {
  ted.raw <- readRDS("ted_raw.Rda")
} else {
  ted.raw <- dist(t(cava@assays$ADT@counts)) # raw
  saveRDS(ted.raw, file = "ted_raw.Rds")
}

fac <- as.factor(md$Cluster)
library(ClassDiscovery)
f <- "spca3.Rda"
if (file.exists(f)) {
  load(f)
} else {
  spca.scaled <- SamplePCA(cava@assays$ADT@scale.data, split = fac)
  spca.norm <- SamplePCA(cava@assays$ADT@data, split = fac)
  spca.raw <- SamplePCA(cava@assays$ADT@counts, split = fac)
  save(spca.raw, spca.norm, spca.scaled, file = f)
}
rm(f)


spackle <- function(fac) {
  opar <- par(mfrow = c(1, 3))
  on.exit(par(opar))
  plot(spca.scaled, main="Scaled", split = fac, col = de24)
  plot(spca.norm, main="Normalized", split = fac, col = de24)
  plot(spca.raw, main="Raw", split = fac, col = de24)
  invisible(fac)
}

kclust.raw <- kmeans(t(as.matrix(cava@assays$ADT@counts)), 17, iter.max = 100)
kclust.norm <- kmeans(t(as.matrix(cava@assays$ADT@data)), 17, iter.max = 100)
kclust.scaled <- kmeans(t(as.matrix(cava@assays$ADT@scale.data)), 17, iter.max = 100)

spackle(fac.raw <- as.factor(kclust.raw$clust))
spackle(fac.norm <- as.factor(kclust.norm$clust))
spackle(fac.scaled <- as.factor(kclust.scaled$clust))
spackle(fac)

rna <- cava@assays$RNA@data
thab <- cava@assays$thAb@data
adt <- cava@assays$ADT@counts
cd4data <- rbind(adt, rna)
tilt <- t(as.matrix(adt))
dim(tilt)


all(colnames(rna) == colnames(adt))  # true
all(colnames(rna) == colnames(thab)) # true

all(rownames(adt) == rownames(thab)) # true



## Re-cluster into 17 groups using K-means
set.seed(94736)
kclust <- kmeans(tilt, 17, iter.max = 100)
table(fac2 <- as.factor(kclust$cluster))
tong <- kclust$centers
dexer <- apply(tong, 1, function(x) {
  foo <- apply(sweep(tilt, 2, x, "-")^2, 1, sum)
  which.min(foo)
})

image(tab <- table(fac, fac2))

library(Mercator)
if ( file.exists("p2.Rda")) {
  load("p2.Rda")
} else {
  if (file.exists("p1.Rda")) {
    load("p1.Rda")
  } else {
    if (file.exists("p0.Rda")) {
      load("p0.Rda")
    } else {
      pluto <- Mercator(ted.norm, "euclid", "hclust", K = 5)
      pluto <- setClusters(pluto, as.numeric(as.factor(md$Cluster)))
      save(pluto, file = "p0.Rda")
      ## Note that a copy of "ted" is now contained in "pluto"
      rm(ted.raw, ted.norm, ted.scaled)
      gc()
    }
##    M <- cmdscale(pluto@distance)
##    pluto@view[["mds"]] <- M
    pluto@view[["mds"]] <- spca.norm@scores[, 1:2]
    rm(spca.raw, spca.scaled, spca.norm)
    library(Rtsne)
    R <- Rtsne(pluto@distance, is_distance = TRUE)
    pluto@view[["tsne"]] <- R
    save(pluto, file = "p1.Rda")
    rm(R)
    gc()
  }
  library(umap)
  U <- umap(tilt)
  pluto@view[["umap"]] <- U
  save(pluto, file = "p2.Rda")
  rm(U)
  gc()
}


fac3 <- factor(cutree(pluto@view[["hclust"]],  k = 17))
pluto@palette <- de24

pluto <- setClusters(pluto, as.numeric(as.factor(md$Cluster)))
pluto <- setClusters(pluto, kclust$cluster)
plot(pluto, view = "umap", main = "UMAP")
text(pluto@view[["umap"]]$layout[dexer,], as.character(1:17), col = "yellow", cex=2)
#points(pluto@view[["umap"]]$layout[dexer,], pch = 17, col = "yellow", cex=2)
plot(pluto, view = "tsne", main = "t-SNE")
text(pluto@view[["tsne"]]$Y[dexer,], as.character(1:17), col = "yellow", cex=2)
#points(pluto@view[["tsne"]]$Y[dexer,], pch = 17, col = "yellow", cex=2)
plot(pluto, view = "mds", main = "MDS/PCA")
text(pluto@view[["mds"]][dexer,], as.character(1:17), col = "yellow", cex=2)
barplot(pluto)

## Looks like there might be some room for improvement
f <- "silly.Rda"
if (file.exists(f)) {
  load(f)
} else {
  library(SillyPutty)
#  sp <- SillyPutty(kclust$cluster, pluto@distance) # takes a __long__ time
#  silh <- silhouette(getClusters(pluto), pluto@distance)
#  save(sp, silh, file = f)
}
rm(f)

#pluto <- setClusters(pluto, sp@cluster)

plot(pluto, view = "hclust")



library(circlize)
J <- 46
X <- log(1+tilt[,J])
hist(X, breaks = 123, main = colnames(tilt)[J])
cr <- colorRamp2(breaks = c(min(X), median(X), max(X)),
                 col = c("cyan", "gray", "red"))
V <- pluto@view[["tsne"]]$Y
plot(V, col = cr(X), pch = 16, main = colnames(tilt)[J])

V <- pluto@view[["umap"]]$layout
colnames(V) <- c("X", "Y")
plot(V, col = cr(X), pch = 16, main = colnames(tilt)[J])

fct <- factor(pluto@clusters)
layout(matrix(1:2, ncol=2), h = 1, w = c(10,3))
plot(pluto, view = "tsne")
plot(0, 0, type = "n")
legend("topleft", levels(fct), col = pluto@palette[1:21],
       pch = 16, cex=1.5)
par(mfrow =c (1,1))

dim(cd4data)
tag <- aggregate(as.data.frame(t(cd4data)), list(fct),
                 function(x) {
                   if (all(x == 0)) {
                     val <- 0
                   } else {
                     y <- x[x>0]
                     val <- mean(y)
                   }
                   val
                 })
class(tag)
tag <- as.matrix(tag[, -1])
dim(tag)
foo <- apply(tag, 2, function(x) sum(x==0))

image(tag)

heron <- function(a, b, c) {
  s <- (a + b+ c)/2
  area <- sqrt(s*(s-a)*(s-b)*(s-c))
  area
}

pd <- as.matrix(pluto@distance)

C <- pd[dexer[6], dexer[13]]
A <- pd[dexer[6],]
B <- pd[dexer[13],]

areas <- heron(A, B, C)
heights <- 2*areas/C
hist(heights, breaks = 234)
condo <- heights < 700 & A < 4000 & B < 4000
plot(V, col = 1+1*(condo), pch=16)
points(V[condo,], col = "red", pch = 16)
points(1.656, -10.998, col = "yellow", pch=17, cex=1.6)
points(3.567, -12.017, col = "yellow", pch=17, cex=1.6)

poof <- cd4data[,condo]
poof <- poof[, order(A[condo])]
nz <- apply(poof, 1, function(x) sum(x > 0))
plot(nz)

heatmap(log(1 + poof[2:49,]))


```


# Appendix
This analysis ws performed in the followking directory:
```{r where}
getwd()
```
It was performed in the following environment;
```{r si}
sessionInfo()
```

