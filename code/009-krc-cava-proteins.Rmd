---
title: "Protein Expression, Ley CAVA Data"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: readable
    highlight: kate
    toc: yes
  pdf_document:
    highlight: kate
    toc: yes
always_allow_html: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE, fig.width=6, fig.height=6, dev = "png")
options(width = 92)

.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Background
We have an idea that we think may lead to better quantifications of mRNA-Seq
data. We want to test it on a real-world data set. Here we use a hierarchical
Bayesian beta-binomial model to estimate the probability that an mRNA read
from a patient sample comes from a particular gene, for all 487 genes in the
data set from Klaus Ley.

```{r setPaths}
source("00-paths.R")
paths
```

# Data
We first load the data set obtained from the laboratory of Dr. Klaus Ley. 
```{r fetchData}
load(file.path(paths$scratch, "LeyData.Rda"))
ls()
```

# Bimodal Index
All of the protein data should, in principle, consist of mixtures of two
distributions, representing cell samples that either do or do not express
the protein. Here we use a tool we developed a long time ago to find those
mixtures along with an optimal cut point. We use log-transformed expression
data for this analysis. Note that we have to manually adjust one cutpoint
since CD4 has a trimodal, and not just bimodal, expression pattern.
```{r BI}
LP <- log2(1 + protein)
temp <- sapply(strsplit(colnames(LP), ".A[HS]"), function(X) X[1])
colnames(LP) <- temp
rm(temp)
library(BimodalIndex)
f <- file.path(paths$scratch, "BI.Rda")
if (file.exists(f)) {
  load(f)
} else {
  library(mclust)
  mc <- Mclust(LP[, "CD4.CD4"], G = 3, modelNames = "E", vebose = FALSE)
  mu <- mc$parameters$mean
  BI <- bimodalIndex(t(LP))
  cutpoint <- with(BI, (mu1 + mu2)/2)
  frac <- sapply(1:ncol(LP), function(J) mean(LP[,J] < cutpoint[J]))
  names(frac) <- names(cutpoint) <- rownames(BI)
  cutpoint["CD4.CD4"] <- (mu[2] + mu[3])/2
  save(BI, cutpoint, frac, mc = mc, file = f)
}
rm(f)
```
There are also several proteins that do not have bimodal patterns;
in this case, the cutpoint is typically set near the mean or median
of the distribution. They are listed here:
```{r unimod}
rownames(BI)[which(BI$BI < 1)]
```

# Dimension Reduction
We want to use nonlinear dimension reduction methods (UMAP and t-SNE)
to visualize the protein expression in different cells.
```{r umap}
library(umap)
library(Rtsne)
f <- file.path(paths$scratch, "dimred.Rda")
if (file.exists(f)) {
  load(f)
} else {
  UU <- umap(protein)
  TT <- Rtsne(protein)
  save(UU, TT, file = f)
}
rm(f)
```
**Figure 2** shows the basic UMAP and t-SNE plots, derived from the
protein data.
```{r fig02, fig.cap=.tag(2, "UMAP and t-SNE plots derived from protein data."), fig.width=12, fig.height=6}
opar <- par(mfrow = c(1,2), bg = "white")
plot(UU$layout, col = "gray", main = "UMAP", xlab = "U!", ylab = "U2")
plot(TT$Y, col = "grey",main = "t-SNE", xlab = "T1", ylab = "T2")
par(opar)
ignore <- dev.copy(png, file = file.path(paths$figures, "UT.png"),
                   width = 12*300, height=6*300, res=300, bg = "white")
ignore <- dev.off()
```

```{r milnor, eval = FALSE}
milnor <- function(D1, D2) {
  scale <- log(D2/D1)
  scale <- scale[!is.infinite(scale)]
  distortion <- diff(range(scale))
  list(scale = scale, distortion = distortion)
}
f <- file.path(paths$scratch, "D1.Rda")
if (file.exists(f)) {
  load(f)
} else {
  picker <- sample(nrow(protein), 10000)
  D1 <- dist(protein[picker,])
  Mumap <- milnor(D1, dist(UU$layout[picker,]))
  Mtsne <- milnor(D1, dist(TT$Y[picker,]))
  save(D1, Mumap, Mtsne, file = f)
}
rm(f)
Mumap$distortion
Mtsne$distortion
```

Now, as noted above, we want to display the expression if individual
proteins on these two-dimensional cell layouts (**Figures 3**). We
use the following function for this purpose.
```{r vispacks}
suppressPackageStartupMessages( library(circlize) )
library(viridisLite)
showP <- function(NM) {
  short <-strsplit(NM, ".A[HS]")[[1]][1]
  X <- LP[, NM]
  Q <- quantile(X, c(0, 25, 50, 75, 100)/100)
  cr <- colorRamp2(Q, viridis(5))
  H <- hist(X, plot = FALSE, breaks=30)
  plot(H, col = cr(H$mids), main = short)
  abline(v = cutpoint[NM], col = "firebrick", lwd=2)
  plot(UU$layout, col = cr(X), main = short)
  plot(TT$Y, col = cr(X), main = short)
}
```

```{r fig03, fig.cap=.tag(3, "Protein Expression."), fig.width=15, fig.height=5}
opar <- par(mfrow =c (1,3))
for (N in colnames(LP)) {
  showP(N)
}
par(opar)

```

# Appendix

This analysis was performed in the following R environment:
```{r si}
sessionInfo()
```
It was also performed at the following on-disk location:
```{r cwd}
getwd()
```