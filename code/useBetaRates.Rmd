---
title: "Applying Beta-Binomial Models to scRNA-Seq Data"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  pdf_document:
    highlight: kate
    toc: yes
  html_document:
    theme: readable
    highlight: kate
    toc: yes
always_allow_html: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE, fig.width=6, fig.height=6)
options(width = 92)

.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Introduction
The idea described here is that we can potentially model then observed data
from a single gene  using separate beta-binomial distributions for each group
in the data set. We will illustrate how to fit a model to test this idea (on
data that we simulated previously). We will also try to provide more details
on how and  why this method is expected to work.

# Simulated Data
We restore the simulated data from the previous report (`simSC1Gene`).
```{r load}
load("smallSim.RDa")
ls()
```

The `BetaModels` package is not yet on CRAN, but it is available from R-Forge
at https://r-forge.r-project.org/R/?group_id=1746. You should be able to
install it using the following (conditional) command:
```{r libpack}
if (!require(BetaModels)) {
  install.packages("BetaModels", repos="http://R-Forge.R-project.org")
}
library(BetaModels)
```

Here is an illustration of how to apply the `BetaRates` function (and classes)
in the `BetaModels` package to one of the previously simulated data sets. Note
that this code only runs the application on one of the simulated patient
samples. There should be no difficulty in iterating over all 21 of the
simulated samples.
```{r fig01, fig.cap=.tag(1, "Posterior distribuiton of X-Y parameters.")}
J <- 1
{
  cen <- guessCenter(Rates1[[J]])
  br <- BetaRates(Counts1[[J]], Total[[1]], 
                  x = cen$X + seq(-0.1, 0.1, length = 100),
                  y = cen$Y + seq(-0.1, 0.2, length = 100))
  image(br)
  S <- summary(br)
  S
}

```

## All Samples
```{r beta-model-fit}
modfits1 <- t(sapply(1:np[1], function(J) {
  cen <- guessCenter(Rates1[[J]])
  br <- BetaRates(Counts1[[J]], Total[[J]], 
                  x = cen$X + seq(-0.1, 0.1, length = 100),
                  y = cen$Y + seq(-0.1, 0.2, length = 100))
  x <- BetaModels:::xform(ee <- BetaModels:::expext(br))
  c(unlist(ee), unlist(x))
}))

modfits2 <- t(sapply(1:np[2], function(J) {
  cen <- guessCenter(Rates2[[J]])
  br <- BetaRates(Counts2[[J]], Total[[J + np[1]]], 
                  x = cen$X + seq(-0.1, 0.1, length = 100),
                  y = cen$Y + seq(-0.1, 0.2, length = 100))
  x <- BetaModels:::xform(ee <- BetaModels:::expext(br))
  c(unlist(ee), unlist(x))
}))

mf <- as.data.frame(rbind(modfits1, modfits2))
gcol <- rep(c("firebrick", "forestgreen"), time = np)
summary(mf)
```

```{r fig02, fig.cap=.tag(2, "Posterior parameters by sample.")}
plot(mf[, c("x", "y")], pch = 16, col = gcol, cex = 2,
     main = "Posterior X and Y")
```

```{r fig03, fig.cap=.tag(3, "Maximum a posteriori eestimates of theta.")}
plot(mf$mean, pch = 16, col = gcol, cex = 2, 
     main = "Estimates of theta")
```

# Model Explanation
We are only interested in a single gene G. The data we obtain from one cell
in one patient-sample about this gene G consists of two items: the total number,
$N$, of (mappable) sequencing reads coming from this cell, and the count, $k$, 
of the number of those reads that can be successfully mapped to the gene $G$ of
interest. That is, we observe the pair $(k, N)$.

Across the entire patient-sample, we observe vectorized versions of this data;
that is, we get $\bar{k} = (k_1, k_2, \dots, k_J)$ and $\bar{N} = (N_1, N_2, 
\dots, N_J)$ where $J$ is the number of cells assayed from that patient-sample.

We can think of each pair $(k_j, N_j)$ as the result of a kind of binomial
experiment, where we make $N_j$ trials (i.e., sequencing reads) and "success" is 
defined as the read being mapped to the gene $G$ of interest. Moreover, the
ratio $k_j/N_j$ is a naive estimate of the "probability of success", $\theta$,
a critical parameter of the binomial distribution (often designated as 
$\textrm{Binom}(N, \theta)$).

Now, a simple-minded model would assume that the probability $\theta$ is the same
constant in all cells from the same patient-sample. (Or at least constant for all
cells of the same type.) Even if that is true, however,
we don't know the value of $\theta$ for any gene in any cell type in any patient. So,
at the very least, we have to learn it from the data. As a result, the observed
data is probably more variable than one would expect simply from using a binomial
model.

A standard way to deal with this increased variability is to assume that there
are different values for $\theta$ in different cells, but that those $\theta$'s
arise from a common beta distribution (which is everybody's favorite distribution
supported in the interval $[0,1]$). The family of beta distributions depend on
a pair of parameters, $\alpha$ and $\beta$. In other words, we use a hierarchical
model, where the first level is defined by takinf the rando cout variable $X$ to be
$$X \sim \textrm{Binom}(N, \theta)$$
and the second level takes
$$\theta \sim \textrm{Beta}(\alpha, \beta).$$

Combining these steps, we can use the so-called "beta-binomial distribution"
(which we will denote by "BB") and simply have
$$X \sim BB(N, \alpha, \beta),$$
where the intermediate variable $\theta$ (at least temporarily) disappears. If we
let $B$ denote the standard beta function (not the statistical distribution),
then the density function of the beta-binomial distribution is given by
$$BB(x; N, \alpha, \beta) = {N \choose x} \frac{B(x+\alpha, N-x+\beta)}{B(\alpha, \beta)}.$$

If we are given data where we believe all the $\theta$'s come from the same beta
distribution, we can treat everything we just said as a hierarchical Bayesian model. 
Then our task is to use the data (i.e., the collection of $(k, N)$ pairs) to estimate
the unknown parameters $\alpha$ and $\beta$ and the resulting expected value of
$\theta$. However, we have to put a prior distribution on the joint distribution
of $\alpha$ and $\beta$. We want to take the most uninformative prior possible. 
One might think that we want the uniform distribution. Now, that doesn't really
make sense, for a couple of reasons. One of them turns out to be easy to deal with:
the only constraint on $\alpha$ and $\beta$ is that they are positive real numbers.
And the "uniform" distribution on the entire first quadrant of the real plane 
doesn't exist (since it has infinte area). The second reason is more technical, 
but more important. It turns out that using $\alpha$ and $\beta$ is the wrong 
parameterization of the beta distribution, and the uniform (improper) distribution
turns out to be informative.  But statistcicians have worked out the "correct" 
parameterization:
$$X = \log(\alpha/\beta), \qquad\qquad Y = \log(\alpha + \beta).$$
Now thee values can be any real numbers, but consideration of the Fisher
information matrix tells us that they are the right thing to use. It also turns
out that we can compute the expected value (mean) just using $X$, while $Y$ can be
used as a surrogate of the "size" of the data.

Now the procedure is to use the naive estimates (the collection of $k/N$ values)
to guess the "center" of the posterior distribution. Create a grid around that 
point in $X$-$Y$ space. Use the uniform prior on that grid. Evaluate the likelihood 
(that's the formula above for the density of the BB distribution) of the observed
data for each value in the grid. Since the prior is uniform (i.e., constant) on
'the grid, the Bayesian rulke "Posterior $\propto$ Prior*Likelihood" means
we can throw away the prior and just use the likelihood.

This way to compute things is **literally** a textbook example:

Reference: Gelman, Carlin, Stern, and Rubin. Bayesian Data Analysis, 2nd edition.
Chapman and HJall/CRC, 2004. (See Section 5.3 in particular.)

```{r echo=FALSE, eval = FALSE}
Rates <- c(Rates1, Rates2)
library(beanplot)
beanplot(Rates, what = c(1,1,1,0), col = as.list(gcol))
```

# Appendix
```{r si}
sessionInfo()
```

