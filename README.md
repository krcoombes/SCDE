# Single Cell Differential Expression
The purpose of this project is to evaluate methods to determine
differential expression of genes from single cell RNA sequencing data
(scRNA-Seq).

We plan to conduct simulations so that we have known ground truth to
compare agains, since at least one of the metrics we want to use is
the flase discovery rate.

Before simulating anything, however, we plan to draft a protocol that
will explain:

1. How (and why) simulations are performed.
2. What criteria will be used to evaluate the results.
